//
//  AppDelegate.h
//  Main Project
//
//  Created by Apple on 19/03/17.
//  Copyright (c) 2017 ejob. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

